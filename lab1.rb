# frozen_string_literal: true

require './lab1_classes'

dog = Dog.new
dog.name = 'Rick'
dog.bark(3)

puts dog.name

snake = Snake.new
snake.length = 10
snake.name = 'Jax'

puts snake.name, snake.length

cat = Cat.new
cat.name = 'Kitty'
cat.gender = 'Male'
cat.age = 1e10
cat.color = 'Black'

puts cat.name, cat.gender, cat.color, cat.age
