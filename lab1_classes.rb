# frozen_string_literal: true

# general pet
class Pet
  attr_accessor :name, :age, :gender, :color
  def bark(times); end
end

# specified pet - cat
class Cat < Pet
end

# specified pet - snake
class Snake < Pet
  attr_accessor :length
end

# specified pet - dog
class Dog < Pet
  def bark(times)
    puts "Woof!\n" * times
  end
end
